if(APPLE)
  cmake_minimum_required(VERSION 2.8.11 FATAL_ERROR)
else()
  cmake_minimum_required(VERSION 2.8.7 FATAL_ERROR)
endif()

project(memmap)

add_library(memmap src/memmap.cpp)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set_property(TARGET memmap PROPERTY CXX_STANDARD 11)
set_property(TARGET memmap PROPERTY CXX_STANDARD_REQUIRED ON)

add_subdirectory(test)
