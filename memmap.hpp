#ifndef __MEMMAP_HPP__
#define __MEMMAP_HPP__

#include <string>
#include <sys/stat.h>
#include <stdint.h>

namespace MemMap {
  /** Type of Access */
  enum class FMode {
    /** Read Only */
    RO,
    /** Write Only */
    WO,
    /** Read & Write */
    RW
  };

  /**
   * This class contains the memory map of a file.
   */
  class Map {
    // Since users can unmap a file before destroying the object,
    // functions like sync() and remap() need a way to check
    // if a mapping is valid.
    int         _fd          = -1;
    bool        _mapped      = false;   
    uint8_t*    _map         = nullptr;
    std::string _path        = "";
    size_t      _size        = 0;

  public:
    void        init(std::string path, FMode mode, size_t added);
    void        remap(FMode mode, size_t added);
    void        sync(bool no_except = false);
    void        unmap(bool no_except = false);

    std::string path();
    size_t      size();

    uint8_t&    operator[](size_t idx);
  
    Map(std::string path, FMode mode, size_t added);
    Map(std::string path, FMode mode);
    Map();
  
    ~Map();
  };
}

#endif
