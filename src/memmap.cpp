#include <string>
#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdexcept>
#include <memmap.hpp>

std::string getRealPath(std::string path, bool maybe_empty);

namespace MemMap {
  /*
   * Public Methods
   */
  
  /**
   * This function initializes a memory mapped file.
   * @param path file path
   * @param mode whether the file should be mapped for reading and/or writing
   * @param added number of bytes with which to pad file 
   */
  void Map::init(std::string path, FMode mode, size_t added) {
    std::string fname = "Map::init()";
  
    // Ensure object does not already have a file
    if (this->_fd != -1) {
      throw std::runtime_error(fname + ": File already initialized: " + this->_path);
    }
  
    // Open output file for reading
    switch(mode) {
    case FMode::RO:
      this->_path = getRealPath(path, false);
      this->_fd   = open(this->_path.c_str(), O_RDONLY);
      break;
    case FMode::WO:
      this->_path = getRealPath(path, true);
      this->_fd   = open(this->_path.c_str(),
  		      O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
      break;
    case FMode::RW:
      this->_path = getRealPath(path, true);
      this->_fd   = open(this->_path.c_str(),
  		      O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
      break;
    default:  
      throw std::runtime_error(fname + ": Invalid mode");
    }
  
    if (this->_fd == -1) {
      throw std::runtime_error(fname + ": Cannot open file: " + path);
    }
    
    this->remap(mode, added);
  }
  
  /**
   * This method remaps the file with a new mode and with additional space appended.
   * @param mode whether the file should be mapped for reading and/or writing
   * @param added number of bytes with which to pad file 
   */
  void Map::remap(FMode mode, size_t added) {
    std::string fname = "Map::remap()";
  
    // Ensure object has open file
    if (this->_fd == -1) {
      throw std::runtime_error(fname + ": File not open: " + this->_path);
    }
  
    // Unmap file if mapped
    if (this->_mapped == true) {
      this->unmap();
    }
    
    int prot_opts;
    
    switch(mode) {
    case FMode::RO:
      prot_opts = PROT_READ;
      break;
    case FMode::WO:
      prot_opts = PROT_WRITE;
      break;
    case FMode::RW:
      prot_opts = PROT_READ | PROT_WRITE;
      break;
    default:  
      throw std::runtime_error(fname + ": Invalid mode");
    }
  
    // Get current size of file
    struct stat input_info;
    auto stat_status = fstat(this->_fd, &input_info);
    if (stat_status == -1) {
      throw std::runtime_error(fname +
  			    ": Cannot retrieve information on file: " +
  			     this->_path);
    }
    
    this->_size = input_info.st_size;
  
    if ((mode == FMode::WO || mode == FMode::RW) && added > 0) {
      this->_size += added;
      
      auto seek_status = lseek(this->_fd, this->_size-1, SEEK_SET);
      if (seek_status == -1) {
        throw std::runtime_error(fname + ": Cannot seek to end of file: " +
  			       this->_path);
      }
  
      // Write something random to end of file to stretch it
      auto write_status = write(this->_fd, "", 1); 
      if (write_status == -1) {
        throw std::runtime_error(fname + ": Cannot stretch " +
  			       this->_path + " to " +
  			       std::to_string(this->_size) + " bytes");
      }
    }
  
    // Memory map output file
    this->_map = (uint8_t*)mmap(0, this->_size, prot_opts,
  			     MAP_SHARED, this->_fd, 0);
    if (this->_map == MAP_FAILED) {
      throw std::runtime_error(fname + ": Error mapping file: " + this->_path);
    }
  
    this->_mapped = true;
  }
  
  /**
   * This method synchronizes the file contents in memory with the contents on disk.
   * @param no_except (OPTIONAL) whether or not to throw an exception when sync fails
   */
  void Map::sync(bool no_except) {
    std::string fname = "Map::sync()";
  
    // Ensure file is still mapped
    if (this->_mapped == false) {
      if (no_except == false) {
        throw std::runtime_error(fname + ": File not mapped: " +
  			       this->_path);
      }
      return;
    }
  
    auto sync_status = msync(this->_map, this->_size, MS_SYNC);
  
    if (sync_status == -1 && no_except == false) { 
      throw std::runtime_error(fname + ": Could not sync file to disk: " +
  			     this->_path);
    }
  }
  
  /**
   * This method destroys the memory mapping of the file.
   * @param no_except (OPTIONAL) whether or not to throw an exception when unmap fails
   */
  void Map::unmap(bool no_except) {
    std::string fname = "Map::unmap()";
    
    // Ensure file is still mapped
    if (this->_mapped == false) {
      if (no_except == false) {
        throw std::runtime_error(fname + ": File not mapped: " +
  			       this->_path);
      }
      return;
    }
  
    this->sync(no_except); // Sync file to disk before unmapping
  
    auto munmap_status = munmap(this->_map, this->_size);
    if (munmap_status == -1 && no_except == false) {
      throw std::runtime_error(fname + ": Error unmapping file: " +
  			     this->_path);
    }
  
    this->_mapped = false;
    this->_fd     = -1;
  }
  
  /**
   * This function returns the path of the file.
   * @returns file path
   */
  std::string Map::path() {
    return this->_path;
  }
  
  /**
   * This function returns the size of the file.
   * @returns file size (in bytes)
   */
  size_t Map::size() {
    return this->_size;
  }
  
  /**
   * This operator returns the byte at the given index.
   * @param idx the index
   * @returns byte value at given index
   */
  uint8_t& Map::operator[](size_t idx) {
    return this->_map[idx];
  }
  
  /**
   * This is the default constructor method.
   * @param path the filesystem path to the file
   * @param mode whether the file should be mapped for reading and/or writing
   * @param added size of additional space to add 
   */ 
  Map::Map(std::string path, FMode mode, size_t added) {
    this->init(path, mode, added);
  }
  
  /**
   * This is the constructor for not adding any more space.
   * @param path the filesystem path to the file
   * @param mode whether the file should be mapped for reading and/or writing
   */ 
  Map::Map(std::string path, FMode mode) {
    this->init(path, mode, 0);
  }
  
  /**
   * This is the constructor for creating a blank object.
   */
  Map::Map() {}
  
  /**
   * This method ensures the file is unmapped and then closes it.
   */
  Map::~Map() {
    if (this->_mapped == true) {
      this->unmap(true);  // Assume file is unmapped succesfully
    }
  
    close(this->_fd);
  }
}
  
/*
 * Private Methods
 */

std::string getRealPath(std::string path, bool maybe_empty) {
  std::string fname = "getRealPath()";
  bool found_file = false;
  struct stat info;

  // Create C string to hold file path
  char* curpath = new char[path.size()+1];
  strncpy(curpath, path.c_str(), path.size());
  curpath[path.size()] = '\0';

  do {
    auto fd = open(curpath, O_RDONLY, (mode_t)0400);
    if (fd == -1) {
      if (maybe_empty == false) {
        delete[] curpath;
        throw std::runtime_error(fname + ": Cannot open file: " +
				 std::string(curpath));
      }

      return path;
    }

    auto stat_status = fstat(fd, &info);
    if (stat_status == -1) {
      close(fd);
      delete[] curpath;
      throw std::runtime_error(fname +
			       ": Cannot retrieve information on file: " +
			       std::string(curpath));
    }

    if (S_ISREG(info.st_mode)) {
      found_file = true;
      close(fd);
    }
    else if (S_ISLNK(info.st_mode)) {
      delete[] curpath;
      curpath = new char[info.st_size+1];
      auto read_status = read(fd, curpath, info.st_size);

      if (read_status == -1) {
        close(fd);
        delete[] curpath;
        throw std::runtime_error(fname + ": Cannot read from file: " +
				 std::string(curpath));
      }
    }
    else {
      close(fd);
      delete[] curpath;
      throw std::runtime_error(fname +
			       ": Input file is neither regular file nor symbolic link: " +
			       std::string(curpath));
    }
  }
  while (!found_file);

  std::string out(curpath);
  delete[] curpath;
  return out;
}
