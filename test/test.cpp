#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <stdint.h>
#include <memmap.hpp>

struct file_t {
  uint8_t* contents;
  size_t   size;
};

file_t readFile(std::string path) {
  file_t file;

  int fd = open(path.c_str(), O_RDONLY);
  if (fd == -1) {
    throw std::runtime_error("readFile cannot open file: " + path);
  }

  // Get current size of file
  struct stat input_info;
  auto stat_status = fstat(fd, &input_info);
  if (stat_status == -1) {
    throw std::runtime_error("Cannot retrieve information on file: " + path);
  }
  
  file.size     = input_info.st_size;
  file.contents = new uint8_t[file.size];

  read(fd, file.contents, file.size);
  close(fd);

  return file;
}

int main(int argc, char** argv) {
  if (argc != 3) {
    std::cerr << "Usage: " << argv[0] << " dest/file/path src/file/path" << std::endl;
    return -1;
  }
  std::string outpath(argv[1]);
  std::string inpath(argv[2]);

  auto in   = readFile(inpath);

  auto file = MemMap::Map(outpath, MemMap::FMode::RW, in.size);
  auto fsiz = file.size();

  for (size_t i = 0; i < in.size; i++) {
    file[fsiz - in.size + i] = in.contents[i];
  }
  
  for (size_t i = 0; i < fsiz; i++) {
    std::cout << file[i];
  }
  std::cout << std::endl;
  
  file.unmap();
  delete[] in.contents;

  return 0;
}
